#!/usr/bin/env perl

use 5.20.0;
use warnings;

use XML::LibXML;
use Function::Parameters;

my ($file) = @ARGV or die "usage: $0 file.xml";

open my $fh, '<', $file or die "Opening $file: $!";
binmode $fh;

my $xml = XML::LibXML->load_xml(IO => $fh);

my $xpath = '//n:eac-cpf/n:cpfDescription/n:identity/n:entityType';
my $result = match_xpath($xml, $xpath, n => 'urn:isbn:1-931666-33-4');
if (defined $result) {
    say "entityType=$result";
}

my $r2 = match_xpath($xml, '/austlitWork/cpfDescription/identity/entityType');
if (defined $r2) {
    say "entityType2=$r2";
}

fun match_xpath($xml, $xpath, %namespaces) {
    my $xpc = XML::LibXML::XPathContext->new($xml);
    while (my ($prefix, $uri) = each %namespaces) {
        $xpc->registerNs($prefix => $uri);
    }
    my $matches = $xpc->find($xpath);
    return if $matches->size == 0;

    return $matches->get_node(1)->to_literal; # one-based indexing here
}
