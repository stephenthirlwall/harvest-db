IMAGE=postgres:9.3
CONTAINER=huni-harvest-pg

MOUNT=/srv/data/sdt/$CONTAINER
POSTGRES_USER=huni
POSTGRES_PASSWORD=huni
POSTGRES_PORT=54320
POSTGRES_DB=huni-harvest

#HUNI_HARVEST_DSN=DBI:Pg:dbname=test;host=127.0.0.1;port=15432;user=postgres

case $1 in

    start)
        docker run -d \
            --name $CONTAINER \
            -v $MOUNT:/var/lib/postgresql/data \
            -e POSTGRES_DB=$POSTGRES_DB \
            -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
            -e POSTGRES_USER=$POSTGRES_USER \
            -p $POSTGRES_PORT:5432 \
            $IMAGE
        ;;

    stop)
        docker stop $CONTAINER
        docker rm $CONTAINER
        ;;

    logs)
        shift
        docker logs "$@" $CONTAINER
        ;;

    exec)
        shift
        docker exec \
            -v $PWD:/work \
            -w /work \
            $CONTAINER "$@"
        ;;

    bash)
        docker exec -ti $CONTAINER /bin/bash
        ;;

    env)
        echo export HARVEST_TEST_PG=\'DBI:Pg:dbname=$POSTGRES_DB\;host=localhost\;port=$POSTGRES_PORT\;user=$POSTGRES_USER\;password=$POSTGRES_PASSWORD\'
        echo export PGHOST=localhost
        echo export PGPORT=$POSTGRES_PORT
        echo export PGUSER=$POSTGRES_USER
        echo export PGPASSWORD=$POSTGRES_PASSWORD
        echo export PGDATABASE=$POSTGRES_DB
        ;;

    *)
        echo usage: $0 '[start|stop|logs|exec|bash|env]'
        exit 1;
        ;;

esac
