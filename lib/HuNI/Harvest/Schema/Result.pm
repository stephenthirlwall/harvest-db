package HuNI::Harvest::Schema::Result;

use 5.16.0;
use warnings;

use parent 'DBIx::Class::Core';

1;

__END__

=head1 NAME

HuNI::Harvest::Schema::Result - base class for all Result classes

=cut
