package HuNI::Harvest::Schema::ResultSet;

use 5.16.0;
use warnings;
use parent 'DBIx::Class::ResultSet';

use Data::Dumper::Concise;
use Function::Parameters qw( :strict );

method as_hashes {
    $self->search({}, {
        result_class => 'DBIx::Class::ResultClass::HashRefInflator'
    });
}

method dump($fh = \*STDERR) {
    my @all = $self->as_hashes->all;
    print {$fh} Dumper(\@all);
}

1;

__END__

=head1 NAME

HuNI::Harvest::Schema::ResultSet

=head1 DESCRIPTION

This is the base class for all the HuNI::Harvest::Schema resultsets.
Any methods defined here will be available for all resultsets.

=head1 METHODS

=head2 dump

Print out the resultset to STDERR or the provided file handle.

=head2 as_hashes

Return the resultset inflated as hashrefs.


=cut
