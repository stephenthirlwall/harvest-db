use utf8;
package HuNI::Harvest::Schema::Original;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces(
    default_resultset_class => "+HuNI::Harvest::Schema::Original::ResultSet",
);


# Created by DBIx::Class::Schema::Loader v0.07043 @ 2015-07-14 12:22:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:OU6rKrUh8E7qMjuCymnfkQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
