use utf8;
package HuNI::Harvest::Schema::Original::Result::File;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Original::Result::File

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Original::Result>

=cut

use base 'HuNI::Harvest::Schema::Original::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<original.file>

=cut

__PACKAGE__->table("original.file");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'original.file_id_seq'

=head2 filename

  data_type: 'text'
  is_nullable: 0

=head2 harvest_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 content

  data_type: 'bytea'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "original.file_id_seq",
  },
  "filename",
  { data_type => "text", is_nullable => 0 },
  "harvest_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "content",
  { data_type => "bytea", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<unique_filenames_per_harvest>

=over 4

=item * L</harvest_id>

=item * L</filename>

=back

=cut

__PACKAGE__->add_unique_constraint("unique_filenames_per_harvest", ["harvest_id", "filename"]);

=head1 RELATIONS

=head2 harvest

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Original::Result::Harvest>

=cut

__PACKAGE__->belongs_to(
  "harvest",
  "HuNI::Harvest::Schema::Original::Result::Harvest",
  { id => "harvest_id" },
  { is_deferrable => 0, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07043 @ 2015-07-14 18:00:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:skbKtEJGtP1ZkmTmiyFQyw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
