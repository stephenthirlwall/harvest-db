use utf8;
package HuNI::Harvest::Schema::Original::Result::Feed;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Original::Result::Feed

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Original::Result>

=cut

use base 'HuNI::Harvest::Schema::Original::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<original.feed>

=cut

__PACKAGE__->table("original.feed");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'original.feed_id_seq'

=head2 provider_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 type

  data_type: 'original.feedtype'
  is_nullable: 0
  size: 4

=head2 uri

  data_type: 'text'
  is_nullable: 0

=head2 enabled

  data_type: 'boolean'
  default_value: true
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "original.feed_id_seq",
  },
  "provider_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "type",
  { data_type => "original.feedtype", is_nullable => 0, size => 4 },
  "uri",
  { data_type => "text", is_nullable => 0 },
  "enabled",
  { data_type => "boolean", default_value => \"true", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<unique_uri_per_feed>

=over 4

=item * L</provider_id>

=item * L</uri>

=back

=cut

__PACKAGE__->add_unique_constraint("unique_uri_per_feed", ["provider_id", "uri"]);

=head1 RELATIONS

=head2 harvests

Type: has_many

Related object: L<HuNI::Harvest::Schema::Original::Result::Harvest>

=cut

__PACKAGE__->has_many(
  "harvests",
  "HuNI::Harvest::Schema::Original::Result::Harvest",
  { "foreign.feed_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 provider

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Original::Result::Provider>

=cut

__PACKAGE__->belongs_to(
  "provider",
  "HuNI::Harvest::Schema::Original::Result::Provider",
  { id => "provider_id" },
  { is_deferrable => 0, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07043 @ 2015-07-14 17:54:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jruZTiCUGu0ZEu0K4iZZuA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
