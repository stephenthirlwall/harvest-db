use utf8;
package HuNI::Harvest::Schema::Original::Result::Resource;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Original::Result::Resource

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Original::Result>

=cut

use base 'HuNI::Harvest::Schema::Original::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<original.resource>

=cut

__PACKAGE__->table("original.resource");

=head1 ACCESSORS

=head2 harvest_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 content

  data_type: 'bytea'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "harvest_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "content",
  { data_type => "bytea", is_nullable => 0 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<resource_harvest_id_key>

=over 4

=item * L</harvest_id>

=back

=cut

__PACKAGE__->add_unique_constraint("resource_harvest_id_key", ["harvest_id"]);

=head1 RELATIONS

=head2 harvest

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Original::Result::Harvest>

=cut

__PACKAGE__->belongs_to(
  "harvest",
  "HuNI::Harvest::Schema::Original::Result::Harvest",
  { id => "harvest_id" },
  { is_deferrable => 0, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07043 @ 2015-07-14 17:54:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:s7wDlv5puRqAvWhVC/T8nQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
