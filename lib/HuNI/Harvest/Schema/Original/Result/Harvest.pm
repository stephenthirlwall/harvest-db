use utf8;
package HuNI::Harvest::Schema::Original::Result::Harvest;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Original::Result::Harvest

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Original::Result>

=cut

use base 'HuNI::Harvest::Schema::Original::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<original.harvest>

=cut

__PACKAGE__->table("original.harvest");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'original.harvest_id_seq'

=head2 date

  data_type: 'date'
  is_nullable: 0

=head2 success

  data_type: 'boolean'
  is_nullable: 0

=head2 feed_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "original.harvest_id_seq",
  },
  "date",
  { data_type => "date", is_nullable => 0 },
  "success",
  { data_type => "boolean", is_nullable => 0 },
  "feed_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<harvest_per_day>

=over 4

=item * L</feed_id>

=item * L</date>

=back

=cut

__PACKAGE__->add_unique_constraint("harvest_per_day", ["feed_id", "date"]);

=head1 RELATIONS

=head2 feed

Type: belongs_to

Related object: L<HuNI::Harvest::Schema::Original::Result::Feed>

=cut

__PACKAGE__->belongs_to(
  "feed",
  "HuNI::Harvest::Schema::Original::Result::Feed",
  { id => "feed_id" },
  { is_deferrable => 0, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 files

Type: has_many

Related object: L<HuNI::Harvest::Schema::Original::Result::File>

=cut

__PACKAGE__->has_many(
  "files",
  "HuNI::Harvest::Schema::Original::Result::File",
  { "foreign.harvest_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 resource

Type: might_have

Related object: L<HuNI::Harvest::Schema::Original::Result::Resource>

=cut

__PACKAGE__->might_have(
  "resource",
  "HuNI::Harvest::Schema::Original::Result::Resource",
  { "foreign.harvest_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07043 @ 2015-07-14 18:00:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1g9DgAECSHlZFKC02Cy1jQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
