use utf8;
package HuNI::Harvest::Schema::Original::Result::Provider;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Harvest::Schema::Original::Result::Provider

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Harvest::Schema::Original::Result>

=cut

use base 'HuNI::Harvest::Schema::Original::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<original.provider>

=cut

__PACKAGE__->table("original.provider");

=head1 ACCESSORS

=head2 id

  data_type: 'text'
  is_nullable: 0

=head2 name

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "text", is_nullable => 0 },
  "name",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<provider_name_key>

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("provider_name_key", ["name"]);

=head1 RELATIONS

=head2 feeds

Type: has_many

Related object: L<HuNI::Harvest::Schema::Original::Result::Feed>

=cut

__PACKAGE__->has_many(
  "feeds",
  "HuNI::Harvest::Schema::Original::Result::Feed",
  { "foreign.provider_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07043 @ 2015-07-14 16:46:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:UGOuqKC/7r1kqCVNzfXusw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
