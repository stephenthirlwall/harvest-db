package HuNI::Harvest::Preprocess;

use 5.20.0;
use warnings;

use Function::Parameters qw( :strict );
use Log::Any qw( :log );
use Try::Tiny;
use XML::LibXML;

fun preprocess($original) {
    try {
        my $xml = XML::LibXML->load_xml(string => $original->data);

        my $provider = $original->provider->name;
        my $preprocessor = get_preprocessor($provider)
            or die "Cannot find preprocessor for $provider\n";

        my $result = $preprocessor->($xml);

        # Remove any existing clean records for this original.
        $original->cleans->delete;

        if (ref $result eq 'ARRAY') {
            # This original document has created multiple records.
            for my $clean (@$result) {
                $original->create_related(clean => {
                    provider_type => $clean->{type},
                    provider_id   => $clean->{id},
                    data          => $clean->{data},
                });
            }
        }
        elsif (ref $result eq 'HASH') {
            # This original document is the record. We don't specify data here
            # as we use the data from the original document.
            $original->create_related(clean => {
                provider_type => $clean->{type},
                provider_id   => $clean->{id},
            });
        }
        else {
            die "Unexpected result $result from preprocessor";
        }
        $original->update({ status => 'clean' });
    }
    catch {
        my $desc = join(' ', $original->provider->name,
                             $original->filename,
                             $original->date);

        chomp;
        $log->error("Error preprocessing $desc: $_\n");
        $original->update({ status => 'invalid' });
    };
}
