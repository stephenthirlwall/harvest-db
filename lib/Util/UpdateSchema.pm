package Util::UpdateSchema;

use 5.20.0;
use warnings;

use Moo;

use DBIx::Class::Schema::Loader 'make_schema_at';
use Log::Any qw( $log );
use Function::Parameters qw( :strict );
use Test::PostgreSQL;

use namespace::clean;

has db_schema => (
    is => 'ro',
    predicate => 1,
);

has namespace => (
    is => 'ro',
    required => 1,
);

has options => (
    is => 'lazy',
    builder => sub { +{ } },
);

has pg_args => (
    is => 'lazy',
    default => sub {
        [qw( --encoding=utf8 )],
    },
);

has pg => (
    is => 'lazy',
    builder => method { Test::PostgreSQL->new(initdb_args =>
        join(' ', $Test::PostgreSQL::Defaults{initdb_args},
                  @{ $self->pg_args })
    )},
);

use Exporter qw( import );
our @EXPORT_OK = qw( update_schema );

sub update_schema {
    my $ok = __PACKAGE__->new(@_)->run;
    return $ok;
}

method run {
    $log->info('Creating fresh database instance');
    $self->deploy_database;
    $self->generate_schema;
    return 1;
}

fun cmd(@cmd) {
    if (system(@cmd) != 0) {
        die "@cmd failed - aborting schema update\n";
    }
}

method deploy_database {
    my $port = $self->pg->port;
    my $user = 'postgres';
    my $host = 'localhost';
    my $dbname = 'test';
    my $uri = "db:pg://$user\@$host:$port/$dbname";
    cmd(qw( sqitch deploy ), $uri);
    cmd(qw( sqitch revert -y ), $uri);
    cmd(qw( sqitch deploy ), $uri);
}

method generate_schema {
    my $namespace = $self->namespace;
    my $args = {
        # use custom result & resultset base classes
        default_resultset_class => "+${namespace}::ResultSet",
        result_base_class => "${namespace}::Result",

        dump_directory  => './lib',
        components      => [qw( InflateColumn::DateTime )],

        # other options
        %{ $self->options },
    };

    if ($self->has_db_schema) {
        # prefix tables with the given schema
        $args->{db_schema} = $self->db_schema,
        $args->{qualify_objects} = 1,
    };

    make_schema_at($namespace, $args, [ $self->pg->dsn ]);
}

1;

__END__

=head1 NAME

Util::UpdateSchema - Create or update the DBIx::Class schema using sqitch.

=head1 SYNOPSIS

    use Util::UpdateSchema;
    my $update = Util::UpdateSchema->new(
        db_schema => 'huni',
        namespace => 'HuNI::Backend::Schema',
    );
    my $ok = $update->run();

    # or

    use Util::UpdateSchema qw( update_schema );
    exit update_schema(
        db_schema => 'huni',
        namespace => 'HuNI::Backend::Schema',
    );

=head1 OPTIONS

=over

=item namespace

Perl namespace for the schema classes. Will have C<::Result> and C<::ResultSet>
appended.

eg. C<HuNI::Backend::Schema>

=item db_schema

Optional schema to use within PostgreSQL.

eg. C<huni>

=cut
