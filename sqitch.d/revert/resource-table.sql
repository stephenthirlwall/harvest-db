-- Revert resource-table

BEGIN;

DROP TABLE IF EXISTS original.resource;

COMMIT;
