-- Revert original-schema

BEGIN;

DROP SCHEMA IF EXISTS original;

COMMIT;
