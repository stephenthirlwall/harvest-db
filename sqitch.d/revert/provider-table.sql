-- Revert provider-table

BEGIN;

DROP TABLE IF EXISTS original.provider;

COMMIT;
