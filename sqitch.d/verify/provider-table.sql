-- Verify provider-table

BEGIN;

SELECT
    id,
    name
  FROM original.provider
  WHERE FALSE;

ROLLBACK;
