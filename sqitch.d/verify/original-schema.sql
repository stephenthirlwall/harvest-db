-- Verify original-schema

BEGIN;

SELECT pg_catalog.has_schema_privilege('original', 'usage');

ROLLBACK;
