-- Verify harvest-table

BEGIN;

SELECT
    id,
    feed_id,
    date,
    success
  FROM original.harvest
  WHERE false;

ROLLBACK;
