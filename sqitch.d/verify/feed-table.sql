-- Verify feed-table

BEGIN;

SELECT
    id,
    provider_id,
    type,
    uri
  FROM original.feed
  WHERE false;

ROLLBACK;
