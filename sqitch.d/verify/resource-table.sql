-- Verify resource-table

BEGIN;

SELECT
    harvest_id,
    content
  FROM original.resource
  WHERE false;

ROLLBACK;
