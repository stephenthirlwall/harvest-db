-- Verify file-table

BEGIN;

SELECT
    id,
    filename,
    harvest_id,
    content
  FROM original.file
  WHERE false;

ROLLBACK;
