-- Deploy feed-table
-- requires: feedtype-enum

BEGIN;

CREATE TABLE original.feed (
    id          serial PRIMARY KEY,
    provider_id text NOT NULL REFERENCES original.provider(id)
                        ON DELETE RESTRICT ON UPDATE RESTRICT,
    type        original.feedtype NOT NULL,
    uri         text NOT NULL,
    enabled     boolean DEFAULT true,

    CONSTRAINT unique_uri_per_feed UNIQUE(provider_id, uri)
);

COMMIT;
