-- Deploy file-table
-- requires: harvest-table

BEGIN;

CREATE TABLE original.file (
    id          serial PRIMARY KEY,
    filename    text NOT NULL,
    harvest_id  integer NOT NULL REFERENCES original.harvest(id)
        ON DELETE RESTRICT ON UPDATE RESTRICT,
    content     bytea NOT NULL,

    CONSTRAINT unique_filenames_per_harvest UNIQUE(harvest_id, filename)
);

COMMIT;
