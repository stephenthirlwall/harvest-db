-- Deploy provider-table
-- requires: feedtype-enum

BEGIN;

CREATE TABLE original.provider (
    id          text PRIMARY KEY,
    name        text NOT NULL UNIQUE
);

COMMIT;
