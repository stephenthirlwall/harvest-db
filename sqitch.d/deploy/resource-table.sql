-- Deploy resource-table
-- requires: harvest-table

BEGIN;

CREATE TABLE original.resource (
    harvest_id  integer UNIQUE NOT NULL REFERENCES original.harvest(id)
        ON DELETE RESTRICT ON UPDATE RESTRICT,
    content     bytea NOT NULL
);

COMMIT;
