-- Deploy feedtype-enum
-- requires: original-schema

BEGIN;

CREATE TYPE original.feedtype AS ENUM (
    'simple',
    'oai'
);

COMMIT;
