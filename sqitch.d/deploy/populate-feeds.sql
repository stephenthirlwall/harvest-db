-- Deploy populate-feeds
-- requires: feed-table

BEGIN;

INSERT INTO original.feed ( provider_id, type, uri ) VALUES
	( 'AWAP',       'oai',      'http://oai.esrc.unimelb.edu.au/AWAPnew/provider?verb=ListIdentifiers&metadataPrefix=eac-cpf' ),
	( 'EOAS',       'oai',      'http://oai.esrc.unimelb.edu.au/EOASnew/provider?verb=ListIdentifiers&metadataPrefix=eac-cpf' ),
	( 'EMEL',       'simple',   'http://www.emelbourne.net.au/eac/resources.xml' ),
	( 'FCAC',       'simple',   'http://www.findandconnect.gov.au/ref/act/eac/resources.xml' ),
	( 'FCNA',       'simple',   'http://www.findandconnect.gov.au/ref/australia/eac/resources.xml' ),
	( 'FCNT',       'simple',   'http://www.findandconnect.gov.au/ref/nt/eac/resources.xml' ),
	( 'FCNS',       'simple',   'http://www.findandconnect.gov.au/ref/nsw/eac/resources.xml' ),
	( 'FCQD',       'simple',   'http://www.findandconnect.gov.au/ref/qld/eac/resources.xml' ),
	( 'FCSA',       'simple',   'http://www.findandconnect.gov.au/ref/sa/eac/resources.xml' ),
	( 'FCTS',       'simple',   'http://www.findandconnect.gov.au/ref/tas/eac/resources.xml' ),
	( 'FCVC',       'simple',   'http://www.findandconnect.gov.au/ref/vic/eac/resources.xml' ),
	( 'FCWA',       'simple',   'http://www.findandconnect.gov.au/ref/wa/eac/resources.xml' ),
	( 'GOLD',       'simple',   'http://www.egold.net.au/eac/resources.xml' ),
	( 'SAUL',       'simple',   'http://www.saulwick.info/eac/resources.xml' ),
	( 'WALL',       'simple',   'http://www.wallabyclub.org.au/compendium/eac/resources.xml' ),
	( 'AU-APFA',    'simple',   'http://www.apfa.esrc.unimelb.edu.au/eac/resources.xml' ),
	( 'Bonza',      'simple',   'http://bonzadb.com.au/static/huni/resources.xml' ),
	( 'CAARP',      'simple',   'http://caarp.edu.au/feed/resources.xml' ),
	( 'AusStage',   'simple',   'http://ausstage.edu.au/feed/resources.xml.gz' ),
	( 'CircusOz',   'simple',   'http://circusozdev.eres.rmit.edu.au/huni/feed/resources.xml.gz' ),
	( 'PDSC',       'oai',      'http://catalog.paradisec.org.au/oai/collection?verb=ListIdentifiers&metadataPrefix=rif, PDSC' ),
	( 'PDSC',       'oai',      'http://catalog.paradisec.org.au/oai/item?verb=ListIdentifiers&metadataPrefix=olac, PDSC' ),
	( 'DAAO',       'oai',      'http://oai.daao.org.au/oai/provider?verb=ListIdentifiers&metadataPrefix=eac-cpf' ),
	( 'ADB',        'oai',      'http://adb.anu.edu.au/oai-pmh?verb=ListIdentifiers&metadataPrefix=eac-cpf' ),
	( 'OA',         'oai',      'http://oa.anu.edu.au/oai-pmh?verb=ListIdentifiers&metadataPrefix=eac-cpf' ),
	( 'AMHD',       'simple',   'http://amhd.info/xml/resources.xml' ),
	( 'MAP',        'simple',   'https://mediaarchivesproject.mq.edu.au/xml/resources.xml' ),
	( 'AFIRC',      'simple',   'http://127.0.0.1/partners/afirc/resources.xml' ),
	( 'AustLit',    'simple',   'http://www.austlit.edu.au/static/huniHarvest/agents/huni.xml, AustLit' ),
	( 'AustLit',    'simple',   'http://www.austlit.edu.au/static/huniHarvest/works/huni.xml, AustLit' ),
	( 'MURA',       'oai',      'http://203.0.89.252/uhtbin/OAI-script.pl?verb=ListIdentifiers&metadataPrefix=marc21&set=ABI, MURA' ),
	( 'MURA',       'oai',      'http://203.0.89.252/uhtbin/OAI-script.pl?verb=ListIdentifiers&metadataPrefix=marc21&set=HUNI, MURA' ),
	( 'MURA',       'oai',      'http://203.0.89.252/uhtbin/OAI-script.pl?verb=ListIdentifiers&metadataPrefix=marc21&set=BOOK/SERIALANALYTICS, MURA' ),
	( 'TUGG',	    'simple',   'http://heurist.sydney.edu.au/HEURIST/HEURIST_FILESTORE/HuNI_TUGG/hml-output/resources.xml' );

COMMIT;
