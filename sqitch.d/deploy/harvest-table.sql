-- Deploy harvest-table
-- requires: feed-table

BEGIN;

CREATE TABLE original.harvest (
    id          serial PRIMARY KEY,
    date        date NOT NULL,
    success     boolean NOT NULL,
    feed_id     integer NOT NULL REFERENCES original.feed(id)
                    ON DELETE RESTRICT ON UPDATE RESTRICT,

    CONSTRAINT harvest_per_day UNIQUE(feed_id, date)
);

COMMIT;
