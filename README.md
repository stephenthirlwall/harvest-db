# Harvest

The plan is to replace the existing huni harvest on-disk storage with one or more Postgres databases.

The reasons for this are many:

* there are currently 4.5 million small files, which is fragile and inefficient in both time and space
* metadata is scattered in many places
    * directory names: original / clean / invalid / solr
    * file names: $provider:::$type:::$id
    * resources.xml files
    * inside solr itself
* original data is intermingled with processed data making it difficult to prototype new things without copying the entire original dataset

There will be one database to store the original harvested data. This will be
writable by the harvest process, but read-only to downstream clients.

The harvest database will have the following tables:

* provider (similar to /etc/huni/harvest.cfg)
    * id (eg. "ADB")
    * name (eg. "Australian Dictionary of Biography")
* feed      UNIQ(provider_id, uri)
    * id
    * provider_id
    * harvest method enum(simple, OAI)
    * fetch endpoint URL
* harvest   UNIQ(feed_id, date)
    * id
    * feed_id
    * date
    * status enum/boolean?
* resource  PK( provider_id )
    * id
    * harvest_id
    * content (resources.xml from most recent successful harvest)
* file      PK( provider_id, filename, date )
    * id
    * filename
    * harvest_id
    * content (should NULL indicate a deleted file?)

Each provider can be harvested separately, and will do so in a transaction. If the harvest fails (network failure etc), the whole transaction is aborted.  The harvest table is always updated, but the file and resource tables are only updated on success.

Note that the resource table contains only one entry per provider, that being the most recent successful harvest.

The harvest procedure for simple feeds will be:

1. Begin transaction
    1. Fetch new resources.xml from provider.
    1. Fetch old resources.xml from resource table.
    1. For each new or updated file.
        1. Fetch file from provider.
        1. Insert into file table.
    1. For each deleted file.
        1. Insert into file table as deleted.
    1. Update or create row in resource table with new resources.xml.
1. Write status into harvest table.

The harvest procedure for OAI feeds is similar:

1. Begin transaction
    1. Fetch most recent resource.xml from resource table.
    1. Fetch index from provider from timestamp onwards (or all if no resource).
    1. For each new or updated file.
        1. Fetch file from provider.
        1. Insert into file table.
    1. Update or create row in resource table with new resource.
1. Write status into harvest table.

To think about:

* How to go about developing a new provider.
* Should files be parsed before adding to the database? (ie. file.valid column)
* Should the file table have a harvest_id column

# Preprocessing

The preprocessing phase takes files from the harvest database and writes them
into a separate clean database.

The clean database will have the following tables:

* record    PK( provider_id, filename, date )
    * provider_id
    * filename
    * id          UNIQUE(id, type, date)
    * type
    * date        FK(provider_id, filename, date) -> harvest.file
    * content

The content column in the record table is optional, and only used in the case where a downloaded file creates more than one clean file. If the content column is null, harvest.file.data is used instead.

Another possible approach here is to replace the content column with an xpath which would extract the relevant sub-document. This might prove limiting if the preprocessor needs to do different processing on the original file.

Preprocessing works like so. Presume we have a resultset of files from the harvest.file table.

1. Select a preprocessor based on file.provider.name
1. Parse original.data
    * This shouldn't fail, because already done in harvest, but what if it does?
1. Call preprocessor(xml-object)
    * In the one-to-one case, this will return { type, id }
        1. Add row to clean.record with NULL content
    * In the one-to-many case, this will return [ { type, id, content } ... ]
        1. Add rows to clean.record with non-NULL content

# Transforming and populating the solr database

The transformation stage will take files from the clean database, transform them and insert them into solr.

Currently, document history is obtained by transforming and inserting all documents into solr in order from oldest to newest. Before a document is inserted it is looked up in solr, and if a previous version exists, it is added to the history for the new version.

This is inefficient in two ways, both by transforming old documents just to throw them away, and also by adding an extra solr query before submitting every document.

The new database setup will allow us to index only the most recent version of each document, with the history being queried from the clean table.

Transformation works like so. Presume we have a resultset of files from the clean.record table.

1. Select a transform based on record.provider.name && record.type
1. Apply transform
1. Add record history to solr record
1. Insert to solr

# Harvest Info

The harvest status app will need to be replaced. The harvest and clean databases will both be used.

A simple angular app might be appropriate here, possibly backed by [postgrest apis](https://github.com/begriffs/postgrest), one per database.

# Misc

Things to check:

* character encoding between fetching files, storing in the database, libxml
* check that md5(original.data) == md5 from resources.xml

Both of these seem to be solved by storing the xml data as binary.

Want to be able to show results of harvests: Original / Clean / Invalid / Solr.

Want to be able to select clean documents by provider/type/id[/date]

How hard is it to actually implement the cross-database foreign keys.
